# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 08:30:05 2020

@author: Anders Nielsen
"""

# how to install geopandas https://geopandas.org/install.html
import geopandas as gpd

import os.path
f_in = "O:/Tech_AN/Anders/scripts/01_GITLAB/GROUP_GIS_rutines/shapefile_to_geojson/shapefiles/Ravn_outline.shp"

def API_shp_to_geojson(shapefile_in):
    
    pathname, extension = os.path.splitext(shapefile_in)
    filename = pathname.split('/')[-1]
    outpath = os.path.dirname(pathname)
    json_out = os.path.join(outpath,filename + ".json")
    
    shp_in = gpd.read_file(shapefile_in)
    try:
        print("the shapefile has projection: %s zone: %s" %(shp_in.crs["proj"], shp_in.crs["zone"]))
    except:
        print("crs cannot be accessed for your shapefile")
        print("check if conversion was okay...")
    # the target CRS is always the same for the geojson: 
    dest_crs = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
        
    shp_in.to_crs(dest_crs).to_file(json_out, driver="GeoJSON")

    
    return

API_shp_to_geojson(f_in)

